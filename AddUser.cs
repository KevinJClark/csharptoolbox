using System;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

// Make sure to add "System.DirectoryServices" and "System.DirectoryServices.AccountManagement" references in Visual Studio

namespace AddUser
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (args.Length < 3 || args.Length > 4 || args[0] == "/help" || args[0] == "/h" || args[0] == "/?" || args[0] == "-help" || args[0] == "-h" || args[0] == "--help")
            {
                Usage();
                return;
            }
            else
            {
                bool domaincontext = false;
                bool localcontext = false;
                bool addmin = false;
                string username = null;
                string password = null;
                foreach (string arg in args)
                {
                    if (arg == "--domain")
                    {
                        domaincontext = true;
                    }
                    else if (arg == "--local")
                    {
                        localcontext = true;
                    }
                    else if (arg == "--admin")
                    {
                        addmin = true;
                    }
                    else
                    {
                        if (username == null)
                        {
                            username = arg;
                        }
                        else if (password == null)
                        {
                            password = arg;
                        }
                    }
                }
                if ((domaincontext && localcontext) || !(domaincontext || localcontext)) // either neither --domain or --local was specified or BOTH were specified ???
                {
                    Usage();
                    return;
                }
                else if (localcontext)
                {
                    CreateLocalUser(username, password, username, addmin);
                }
                else if (domaincontext)
                {
                    CreateDomainUser(username, password);

                    AddDomainUserToGroup(username, "Domain Admins");
                }
            }
        }
        static void Usage()
        {
            Console.WriteLine("AddUser -- Creates a new local or domain user account, optionally adding it to the local or domain admins group\n");
            Console.WriteLine("Usage:    AddUser.exe <--local|--domain> [--admin] <username> <password>\n");
            Console.WriteLine("Examples: AddUser.exe --local testuserlocal password123!");
            Console.WriteLine("          AddUser.exe --domain domuser password1234!");
            Console.WriteLine("          AddUser.exe --local --admin mylocaladmin P@ssw0rd");
            Console.WriteLine("          AddUser.exe --domain --admin cla2021ept SecurePassword1234_!");
        }
        static void CreateDomainUser(string accountname, string password)
        {
            using (var pc = new PrincipalContext(ContextType.Domain))
            {
                using (var up = new UserPrincipal(pc))
                {
                    try
                    {
                        up.SamAccountName = accountname;
                        up.EmailAddress = accountname + "@" + Environment.UserDomainName;
                        up.SetPassword(password);
                        up.Enabled = true;
                        up.Save();
                        Console.WriteLine("User " + accountname + " successfully added to \"domain users\" group with password: " + password);
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine("Domain user " + accountname + " could not be created:\n" + e.Message);
                    }
                }
            }
        }
        static void AddDomainUserToGroup(string userId, string groupName)
        {
            using (PrincipalContext pc = new PrincipalContext(ContextType.Domain))
            {
                GroupPrincipal groupDomainUser = GroupPrincipal.FindByIdentity(pc, "Domain Users");
                GroupPrincipal group = GroupPrincipal.FindByIdentity(pc, groupName);

                if(groupDomainUser == null)
                {
                    Console.WriteLine("Couldn't find group: Domain Users");
                    return;
                }
                if(group == null)
                {
                    Console.WriteLine("Couldn't find group: " + groupName);
                    return;
                }

                UserPrincipal user = UserPrincipal.FindByIdentity(pc, IdentityType.SamAccountName, userId);
                if(user == null)
                {
                    Console.WriteLine("Couldn't find user in domain with name of " + userId);
                    return;
                }
                else
                {
                    Console.WriteLine("Trying to add user " + userId + " to group " + groupName + " ...");
                }

                try
                {
                    group.Members.Add(user);
                    group.Save();
                    Console.WriteLine("Added user " + userId + " to group " + groupName + " successfully!");
                }
                catch(Exception e)
                {
                    Console.WriteLine("Couldn't add domain user " + userId + " to group " + groupName + ":\n" + e.Message);
                }
            }

        }
        // Yoink: https://forums.asp.net/t/1057619.aspx?How+do+we+create+a+user+with+admin+rights+in+C+using+DirectoryEntry
        static void CreateLocalUser(string login, string password, string fullName, bool isAdmin)
        {
            try
            {
                DirectoryEntry dirEntry = new DirectoryEntry("WinNT://" + Environment.MachineName + ",computer");
                DirectoryEntries entries = dirEntry.Children;
                DirectoryEntry newUser = entries.Add(login, "user");
                newUser.Properties["FullName"].Add(fullName);
                newUser.Invoke("SetPassword", password);
                newUser.CommitChanges();


                // Remove the if condition along with the else to create user account in "user" group.
                DirectoryEntry grp;
                if (isAdmin)
                {
                    grp = dirEntry.Children.Find("Administrators", "group");
                    if (grp != null) { grp.Invoke("Add", new object[] { newUser.Path.ToString() }); }
                    Console.WriteLine("User " + login + " successfully added to local administrators group with password: " + password);
                }
                else
                {
                    grp = dirEntry.Children.Find("Users", "group");
                    if (grp != null) { grp.Invoke("Add", new object[] { newUser.Path.ToString() }); }
                    Console.WriteLine("User " + login + " successfully added to local users group with password: " + password);
                }

                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
