using System;
using System.Reflection;
using System.IO;


namespace AppDomainLoader
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Write("Command > ");
                string input = Console.ReadLine();
                if (input == "help")
                {
                    Help();
                }
                else if (input.StartsWith("load"))
                {
                    string filepath = input.Split(' ')[1];
                    byte[] asmbytes = File.ReadAllBytes(filepath);
                    string output = Load(asmbytes);
                    Console.WriteLine("Output from assembly:");
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine(output);
                }
                else if (input.StartsWith("appdomainload"))
                {
                    string filepath = input.Split(' ')[1];
                    byte[] asmbytes = File.ReadAllBytes(filepath);
                    string output = AppDomainLoad(asmbytes);
                    Console.WriteLine("Output from assembly:");
                    Console.WriteLine("--------------------------------------------");
                    Console.WriteLine(output);
                }
            }

        }

        private static void Help()
        {
            Console.WriteLine("");
            Console.WriteLine("The following commands are used to load and execute an assembly from a file. Use one of the following:");
            Console.WriteLine("load <filepath>");
            Console.WriteLine("appdomainload <filepath>");
            Console.WriteLine("");
            Console.WriteLine("load will simply load the assembly into the current appdomain.");
            Console.WriteLine("appdomainload will load the assembly into a new appdomain then remove the appdomain afterwards");
            Console.WriteLine("Use the Visual Studio memory profiler to view how the memory usage grows over time");
            Console.WriteLine("Use LoadedModules (https://gitlab.com/KevinJClark/csharptoolbox/-/blob/master/LoadedModules.cs) to view loaded assemblies");
            Console.WriteLine("");

        }

        private static string Load(byte[] assemblyBytes, string argumentString = "  ")
        {

            string Delimeter = "\",\""; // split on quote comma quote ( "," )
            argumentString = argumentString.Trim('"'); //Remove leading and trailing quotes
            string[] assemblyArgs = argumentString.Split(new[] { Delimeter }, StringSplitOptions.None);


            if (argumentString == "  ")
            {
                assemblyArgs = null;
            }
            bool foundMain = false;
            bool execMain = false;
            Assembly assembly = null;

            try
            {
                assembly = Assembly.Load(assemblyBytes);
            }
            catch (Exception e)
            {
                return "[!] Could not load assembly. Error caught:\n" + e.ToString();
            }

            // Get all types in the assembly
            Type[] types = assembly.GetExportedTypes();

            // Run through each type (aka class), finding methods contained within
            foreach (Type type in types)
            {
                // Get all methods in the type
                MethodInfo[] methods = type.GetMethods();

                // Run through each method, searching for Main method (aka function)
                foreach (MethodInfo method in methods)
                {
                    if (method.Name == "Main")
                    {
                        foundMain = true;
                        if (!type.Attributes.HasFlag(TypeAttributes.Abstract))
                        {
                            execMain = true;

                            //Redirect output from C# assembly (such as Console.WriteLine()) to a variable instead of screen
                            TextWriter prevConOut = Console.Out;
                            var sw = new StringWriter();
                            Console.SetOut(sw);

                            object instance = Activator.CreateInstance(type);
                            // https://stackoverflow.com/questions/3721782/parameter-count-mismatch-with-invoke
                            if (argumentString == "  ")
                            {
                                string[] empty = new string[0];
                                method.Invoke(instance, new object[] { empty }); // Runs the main function without args
                            }
                            else
                            {
                                method.Invoke(instance, new object[] { assemblyArgs }); // Runs the main function with args
                            }

                            //Restore output -- Stops redirecting output
                            Console.SetOut(prevConOut);
                            var output = sw.ToString();

                            return (string)output;
                        }
                    }
                }
            }
            if (!foundMain)
            {
                return "[!] No public \"Main()\" function found in assembly. Did you make sure to set the class as public?";
            }
            else if (!execMain)
            {
                return "[!] Found public \"Main()\" function but could not execute it. Make your assembly's CPU arch matches this program.";
            }
            else
            {
                return "[!] Unexpected error occured";
            }

        }


        private static string AppDomainLoad(byte[] assemblyBytes, string argumentString = "  ")
        {

            string Delimeter = "\",\""; // split on quote comma quote ( "," )
            argumentString = argumentString.Trim('"'); //Remove leading and trailing quotes
            string[] assemblyArgs = argumentString.Split(new[] { Delimeter }, StringSplitOptions.None);


            if (argumentString == "  ")
            {
                assemblyArgs = null;
            }
            bool foundMain = false;
            bool execMain = false;
            Assembly assembly = null;
            string random_name = Guid.NewGuid().ToString();
            AppDomain appDomain = AppDomain.CreateDomain(random_name);
            


            try
            {
                
                Console.WriteLine("[*] Created new appdomain named: " + random_name);
                assembly = appDomain.Load(assemblyBytes);
                Console.WriteLine("[*] Loaded in our assembly into the newly created appdomain");
            }
            catch (Exception e)
            {
                return "[!] Could not load assembly. Error caught:\n" + e.ToString();
            }

            // Get all types in the assembly
            Type[] types = assembly.GetExportedTypes();

            // Run through each type (aka class), finding methods contained within
            foreach (Type type in types)
            {
                // Get all methods in the type
                MethodInfo[] methods = type.GetMethods();

                // Run through each method, searching for Main method (aka function)
                foreach (MethodInfo method in methods)
                {
                    if (method.Name == "Main")
                    {
                        foundMain = true;
                        if (!type.Attributes.HasFlag(TypeAttributes.Abstract))
                        {
                            execMain = true;

                            //Redirect output from C# assembly (such as Console.WriteLine()) to a variable instead of screen
                            TextWriter prevConOut = Console.Out;
                            var sw = new StringWriter();
                            Console.SetOut(sw);

                            object instance = Activator.CreateInstance(type);
                            // https://stackoverflow.com/questions/3721782/parameter-count-mismatch-with-invoke
                            if (argumentString == "  ")
                            {
                                string[] empty = new string[0];
                                method.Invoke(instance, new object[] { empty }); // Runs the main function without args
                            }
                            else
                            {
                                method.Invoke(instance, new object[] { assemblyArgs }); // Runs the main function with args
                            }

                            //Restore output -- Stops redirecting output
                            Console.SetOut(prevConOut);
                            var output = sw.ToString();

                            AppDomain.Unload(appDomain); // Unload the appdomain and all the assemblies inside of it after we're finished
                            return (string)output;
                        }
                    }
                }
            }
            if (!foundMain)
            {
                return "[!] No public \"Main()\" function found in assembly. Did you make sure to set the class as public?";
            }
            else if (!execMain)
            {
                AppDomain.Unload(appDomain);
                return "[!] Found public \"Main()\" function but could not execute it. Make your assembly's CPU arch matches this program.";
            }
            else
            {
                AppDomain.Unload(appDomain);
                return "[!] Unexpected error occured";
            }

        }
    }
}
