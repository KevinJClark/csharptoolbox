﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cassia;
using System.Collections;

namespace Msg
{
    class Program
    {
        private static readonly ITerminalServicesManager _manager = new TerminalServicesManager();
        static void Main(string[] args)
        {
            if(args.Length < 3 || args[0] == "-h" || args[0] == "/?" || args[0] == "/h" || args[0] == "--help" || args[0] == "-help" || args[0] == "/help")
            {
                Console.WriteLine("WinBinReplacements: msg.exe [--ask] [--icon <icon>] [--timeout <seconds>] [--session <session_id|all>] [--host <hostname>] <header> <message> <buttontype>");
                Console.WriteLine("\nOptions for icon types are listed below:");
                Console.WriteLine("----------");
                Console.WriteLine("None\nHand\nQuestion\nExclamation\nAsterisk\nWarning\nError\nInformation\nStop\n");
                Console.WriteLine("Options for button types are listed below:");
                Console.WriteLine("----------");
                Console.WriteLine("Ok\nOkCancel\nAbortRetryIgnore\nYesNoCancel\nYesNo\nRetryCancel");
                return;
            }
            bool ask = false;
            string icon = "None";
            string timeout = "20";
            string session = null;
            string host = "local";
            string header = null;
            string message = null;
            string buttontype = null;

            int count = 0;
            foreach (string arg in args)
            {
                if (arg == "--icon")
                {
                    icon = args[count + 1];
                }
                else if (arg == "--timeout")
                {
                    timeout = args[count + 1];
                }
                else if (arg == "--session")
                {
                    session = args[count + 1];
                }
                else if (arg == "--host")
                {
                    host = args[count + 1];
                }
                else if (arg == "--ask")
                {
                    ask = true;
                }
                else if(header == null)
                {
                    header = arg;
                }
                else if (message == null)
                {
                    message = arg;
                }
                else if (buttontype == null)
                {
                    buttontype = arg;
                }
                count++;
            }

            if (session == null)
            {
                session = _manager.CurrentSession.SessionId.ToString();
            }

            // Print out our values just so the user knows what they are
            Console.WriteLine("Ask        : " + ask.ToString());
            Console.WriteLine("Icon       : " + icon);
            Console.WriteLine("Timeout    : " + timeout);
            Console.WriteLine("Session    : " + session);
            Console.WriteLine("Host       : " + host);
            Console.WriteLine("Header     : " + header);
            Console.WriteLine("Message    : " + message);
            Console.WriteLine("ButtonType : " + buttontype + "\n");

            if (ask)
            {
                string[] askargs = { "ask", host, session, icon, header, message, timeout, buttontype };
                AskQuestion(askargs);
            }
            else
            {
                string[] msgargs = { "message", host, session, icon, header, message, timeout, buttontype };
                SendMessage(msgargs);
            }

        }


        private static void AskQuestion(string[] args)
        {
            if (args.Length < 8)
            {
                Console.WriteLine(
                    "Usage: SessionInfo ask [server] [session id] [icon] [caption] [text] [timeout] [buttons]");
                return;
            }
            int seconds = int.Parse(args[6]);
            
            using (ITerminalServer server = GetServerFromName(args[1]))
            {
                server.Open();

                ArrayList sessionIds = new ArrayList();
                if (args[2] == "all")
                {
                    foreach (ITerminalServicesSession session in server.GetSessions())
                    {
                        sessionIds.Add(session.SessionId);
                    }
                }
                else
                {
                    sessionIds.Add(int.Parse(args[2]));
                }

                foreach (int sessionId in sessionIds)
                {
                    try
                    {
                        Console.WriteLine("Sending question to session " + sessionId.ToString());
                        ITerminalServicesSession session = server.GetSession(sessionId);
                        RemoteMessageBoxIcon icon =
                            (RemoteMessageBoxIcon)Enum.Parse(typeof(RemoteMessageBoxIcon), args[3], true);
                        RemoteMessageBoxButtons buttons =
                            (RemoteMessageBoxButtons)Enum.Parse(typeof(RemoteMessageBoxButtons), args[7], true);
                        RemoteMessageBoxResult result =
                            session.MessageBox(args[5], args[4], buttons, icon, default(RemoteMessageBoxDefaultButton),
                                               default(RemoteMessageBoxOptions), TimeSpan.FromSeconds(seconds), true);
                        Console.WriteLine("Response from session: " + sessionId.ToString() + ": " + result);
                    }
                    catch { }
                }
            }
        }

        private static void SendMessage(string[] args)
        {
            if (args.Length < 6)
            {
                Console.WriteLine("Usage: SessionInfo message [server] [session id] [icon] [caption] [text]");
                return;
            }
            using (ITerminalServer server = GetServerFromName(args[1]))
            {
                server.Open();

                ArrayList sessionIds = new ArrayList();
                if (args[2] == "all")
                {
                    foreach (ITerminalServicesSession session in server.GetSessions())
                    {
                        sessionIds.Add(session.SessionId);
                    }
                }
                else
                {
                    sessionIds.Add(int.Parse(args[2]));
                }

                foreach (int sessionId in sessionIds)
                {
                    try
                    {
                        ITerminalServicesSession session = server.GetSession(sessionId);
                        RemoteMessageBoxIcon icon =
                            (RemoteMessageBoxIcon)Enum.Parse(typeof(RemoteMessageBoxIcon), args[3], true);
                        session.MessageBox(args[5], args[4], icon);
                    }
                    catch { }
                }
            }
        }

        private static ITerminalServer GetServerFromName(string serverName)
        {
            return
                (serverName.Equals("local", StringComparison.InvariantCultureIgnoreCase)
                     ? _manager.GetLocalServer()
                     : _manager.GetRemoteServer(serverName));
        }

    }

}
