﻿using System;
using Cassia;

// Requires the Cassia NuGet package

namespace Quser
{
    public class Program
    {
        private static readonly ITerminalServicesManager _manager = new TerminalServicesManager();

        static void Usage()
        {
            Console.WriteLine("Quser -- Queries the logon sessions on a remote host. If no host is specified, queries the local machine");
            Console.WriteLine("Usage: Quser.exe [hostname]");
        }


        public static void Main(string[] args)
        {
            if (args.Length > 0 && (args[0] == "/help" || args[0] == "/h" || args[0] == "/?" || args[0] == "-help" || args[0] == "-h" || args[0] == "--help"))
            {
                Usage();
                return;
            }


            try
            {
                ListSessions(args);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        private static void ListSessions(string[] args)
        {
            string servername = "local";
            if (args.Length == 1)
            {
                servername = args[0];
            }
            using (ITerminalServer server = GetServerFromName(servername))
            {
                try
                {
                    server.Open();
                    var sessions = server.GetSessions();

                    if (servername != "local")
                    {
                        Console.WriteLine("Opening connection to host: " + servername + "\n");
                    }
                    Console.WriteLine("Username               Session Name          Session ID      State           Idle Time       Connected From   Logon Time");
                    Console.WriteLine("=============          ==============        ==========      ============    ============    ==============   ==========");

                    foreach (ITerminalServicesSession session in sessions)
                    {
                        WriteSessionInfo(session);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Could not open server " + servername + " to read session data or data could not be collected:\nError Message: " + e.Message);
                }
            }
        }

        private static ITerminalServer GetServerFromName(string serverName)
        {
            return
                (serverName.Equals("local", StringComparison.InvariantCultureIgnoreCase)
                     ? _manager.GetLocalServer()
                     : _manager.GetRemoteServer(serverName));
        }

        private static void WriteSessionInfo(ITerminalServicesSession session)
        {
            string useraccount = "X";
            string connectedfrom = "X";
            if (session.UserAccount != null)
            {
                useraccount = session.UserAccount.ToString();
            }
            if (session.ClientIPAddress != null)
            {
                connectedfrom = session.ClientIPAddress.ToString();
            }

            Console.Write(useraccount + new string(' ', 23 - useraccount.Length));
            Console.Write(session.WindowStationName + new string(' ', 22 - session.WindowStationName.Length));
            Console.Write(session.SessionId.ToString() + new string(' ', 16 - session.SessionId.ToString().Length));
            Console.Write(session.ConnectionState.ToString() + new string(' ', 16 - session.ConnectionState.ToString().Length));
            Console.Write(session.IdleTime.ToString(@"dd\d\ hh\:mm\:ss") + new string(' ', 16 - session.IdleTime.ToString(@"dd\d\ hh\:mm\:ss").Length));
            Console.Write(connectedfrom + new string(' ', 17 - connectedfrom.Length));
            Console.WriteLine(session.LoginTime.ToString());
        }
    }
}