# No More Shell

**(Windows Binary Replacements)**

You've `shell`ed your last command

A collection of C Sharp binaries intended to help remove an operator's dependance on running built in Windows binaries.

Based off this talk: https://www.youtube.com/watch?v=GHmOJhpMw_o

Current binaries:

* Cacls
* Cat
* Cp
* Dclist
* Decode
* Encode
* Getpid
* Hostname
* Ls
* Mkdir
* Msg
* Netstat
* Ping
* Ps
* Pwd
* Quser
* Rm
* Set
* Taskkill
* Whoami

Note: You'll have to ILMerge some of the binaries (Nslookup, Quser, Msg) that rely on NuGet modules since machines won't have have the dependancies installed

Additional note: I don't plan on adding Reg.exe since [SharpReg](https://github.com/jnqpblc/SharpReg) already exists.
